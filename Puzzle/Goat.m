//
//  Goat.m
//  Fairytale
//
//  Created by Zhenya Kaidashov on 12/14/17.
//  Copyright © 2017 Zhenya Kaidashov. All rights reserved.
//

#import "Goat.h"

@implementation Goat

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.userInteractionEnabled = YES;
        self.location = OnTheLeftShore;
        self.type = GoatHero;
        self.boatConstraints = [NSMutableArray array];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
