//
//  HeroHandler.m
//  Fairytale
//
//  Created by Zhenya Kaidashov on 12/12/17.
//  Copyright © 2017 Zhenya Kaidashov. All rights reserved.
//

#import "HeroHandler.h"

@implementation HeroHandler

#pragma mark - Return hero to the left or right shore

- (void)heroReturn:(Hero *)hero toTheLeftShore:(UIView *)view; {
    
    __block CGRect frame = hero.frame;
    frame.origin = hero.leftShorePosition;
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         hero.frame = frame;
                     }
     ];
}

- (void)heroReturn:(Hero *)hero toTheRightShore:(UIView *)view {
    __block CGRect frame = hero.frame;
    frame.origin = hero.rightShorePosition;
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         hero.frame = frame;
                     }
     ];
}

#pragma mark - Move object to the left or right shore from boat

- (void)heroMove:(Hero *)hero toTheLeftHore:(UIView *)view fromBoat:(Boat *)boat {
    __block CGRect frame = hero.frame;
    frame.origin = [view convertPoint:hero.leftShorePosition toView:(UIImageView *)boat];
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         hero.frame = frame;
                     }
     ];
    [hero removeFromSuperview];
    hero.location = OnTheLeftShore;
    [view addSubview:hero];
    [view addConstraints:hero.leftShoreConstraints];
}

- (void)heroMove:(Hero *)hero toTheRightHore:(UIView *)view fromBoat:(Boat *)boat {
    __block CGRect frame = hero.frame;
    frame.origin = [view convertPoint:hero.rightShorePosition toView:(UIView *)boat];
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         hero.frame = frame;
                     }
     ];
    [hero removeFromSuperview];
    hero.location = OnTheRightShore;
    [view addSubview:hero];
    [view addConstraints:hero.rightShoreConstraints];
}

#pragma mark - Move or return hero to the boat

- (void)heroMove:(Hero *)hero toTheBoat:(Boat *)boat from:(UIView *)view {
    __block CGRect frame = hero.frame;
    frame.origin = [(UIImageView *)boat convertPoint:hero.boatPosition toView:view];
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         hero.frame = frame;
                     }
     ];
    
    [hero removeFromSuperview];
    hero.location = InTheBoat;
    [(UIImageView *)boat addSubview:hero];
    [(UIImageView *)boat addConstraints:hero.boatConstraints];
}

- (void)heroReturn:(Hero *)hero toTheBoat:(Boat *)boat {
    __block CGRect frame = hero.frame;
    frame.origin = hero.boatPosition;
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         hero.frame = frame;
                     }
     ];
}

@end
