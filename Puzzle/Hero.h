//
//  Hero.h
//  Fairytale
//
//  Created by Zhenya Kaidashov on 12/12/17.
//  Copyright © 2017 Zhenya Kaidashov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum {
    OnTheLeftShore,
    InTheBoat,
    OnTheRightShore
} HeroLocation;

typedef enum {
    WolfHero    = 1 << 0,
    GoatHero    = 1 << 1,
    CabbageHero = 1 << 2,
} HeroType;

@class Boat;
@protocol HeroDelegate;

@interface Hero : UIImageView

@property (assign, nonatomic) CGPoint leftShorePosition;
@property (assign, nonatomic) CGPoint rightShorePosition;
@property (assign, nonatomic) CGPoint boatPosition;

@property (strong, nonatomic) NSMutableArray *leftShoreConstraints;
@property (strong, nonatomic) NSMutableArray *rightShoreConstraints;
@property (strong, nonatomic) NSMutableArray *boatConstraints;

@property (assign, nonatomic) HeroLocation location;
@property (assign, nonatomic) HeroType type;
@property (weak, nonatomic) id <HeroDelegate> delegate;

- (void)returnToTheLeftShore:(UIView *)view;
- (void)returnToTheRightShore:(UIView *)view;
- (void)returnToTheBoat:(Boat *)boat;

- (void)moveToTheLeftShore:(UIView *)view fromBoat:(Boat *)boat;
- (void)moveToTheRightShore:(UIView *)view fromBoat:(Boat *)boat;
- (void)moveToTheBoat:(Boat *)boat from:(UIView *)view;

- (void)setLeadingConstraint:(CGFloat)constant toBoat:(Boat *)boat;
- (void)setTopConstraint:(CGFloat)constant toBoat:(Boat *)boat;
- (NSMutableArray *)setConstraintsForRightShore:(UIView *)view;

- (NSMutableArray *)getDefaultConstraintsFromView:(UIView *)view;

@end

@protocol HeroDelegate <NSObject>
@required
- (void)heroReturn:(Hero *)hero toTheLeftShore:(UIView *)view;
- (void)heroReturn:(Hero *)hero toTheRightShore:(UIView *)view;

- (void)heroMove:(Hero *)hero toTheLeftHore:(UIView *)view fromBoat:(Boat *)boat;
- (void)heroMove:(Hero *)hero toTheRightHore:(UIView *)view fromBoat:(Boat *)boat;

- (void)heroReturn:(Hero *)hero toTheBoat:(Boat *)boat;
- (void)heroMove:(Hero *)hero toTheBoat:(Boat *)boat from:(UIView *)view;

@end
