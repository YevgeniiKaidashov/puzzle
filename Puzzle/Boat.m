//
//  Boat.m
//  Fairytale
//
//  Created by Zhenya Kaidashov on 12/12/17.
//  Copyright © 2017 Zhenya Kaidashov. All rights reserved.
//

#import "Boat.h"

NSString *const BoatGoToTheLeftShoreNotification = @"BoatGoToTheLeftShoreNotification";
NSString *const BoatGoToTheRightShoreNotification = @"BoatGoToTheRightShoreNotification";

@implementation Boat

#pragma mark - Init method

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.userInteractionEnabled = YES;
        self.location = AtTheLeftShore;
        
        UISwipeGestureRecognizer *swipeGestureLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleLeftSwipe:)];
        swipeGestureLeft.direction = UISwipeGestureRecognizerDirectionLeft;
        swipeGestureLeft.delegate = self;
        
        [self addGestureRecognizer:swipeGestureLeft];
        
        UISwipeGestureRecognizer *swipeGestureRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleRightSwipe:)];
        swipeGestureRight.direction = UISwipeGestureRecognizerDirectionRight;
        swipeGestureRight.delegate = self;
        
        [self addGestureRecognizer:swipeGestureRight];
    }
    return self;
}

#pragma mark - Handle swipe

- (void)moveToDirection:(BoatDirection)direction {
    CGRect frame = self.frame;
    
    if (direction == BoatDirectionLeft) {
        frame.origin.x = self.startBoatXPosition;
        self.location = AtTheLeftShore;
    } else if (direction == BoatDirectionRight) {
        frame.origin.x = self.endBoatXPosition;
        self.location = AtTheRightShore;
    }
    
    [UIView animateWithDuration:0.3f
                     animations:^{
                         self.frame = frame;
                     }
                     completion:^(BOOL finished) {
                         if (finished) {
                             if (direction == BoatDirectionLeft) {
                                 [[NSNotificationCenter defaultCenter] postNotificationName:BoatGoToTheLeftShoreNotification object:nil userInfo:nil];
                             } else if (direction == BoatDirectionRight) {
                                 [[NSNotificationCenter defaultCenter] postNotificationName:BoatGoToTheRightShoreNotification object:nil userInfo:nil];
                             }
                         }
                     }
     ];
}

- (void)handleLeftSwipe:(UISwipeGestureRecognizer *)swipeGesture {
    [self moveToDirection:BoatDirectionLeft];
}

- (void)handleRightSwipe:(UISwipeGestureRecognizer *)swipeGesture {
    [self moveToDirection:BoatDirectionRight];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRequireFailureOfGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return [gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UISwipeGestureRecognizer class]];
}

#pragma mark - Other methods

- (void)setStartAndFinishMovementPositionBetweenLeft:(UIView *)leftShore andRightShores:(UIView *)rightShore {
    self.startBoatXPosition = CGRectGetMinX(self.frame);
    self.endBoatXPosition = CGRectGetMinX(rightShore.frame) - CGRectGetWidth(self.frame) + 10;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
