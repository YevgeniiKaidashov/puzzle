//
//  Hero.m
//  Fairytale
//
//  Created by Zhenya Kaidashov on 12/12/17.
//  Copyright © 2017 Zhenya Kaidashov. All rights reserved.
//

#import "Hero.h"

@implementation Hero

- (void)returnToTheLeftShore:(UIView *)view {
    if ([self.delegate conformsToProtocol:@protocol(HeroDelegate)]) {
        [self.delegate heroReturn:self toTheLeftShore:view];
    }
}

- (void)returnToTheRightShore:(UIView *)view {
    if ([self.delegate conformsToProtocol:@protocol(HeroDelegate)]) {
        [self.delegate heroReturn:self toTheRightShore:view];
    }
}

- (void)returnToTheBoat:(Boat *)boat {
    if ([self.delegate conformsToProtocol:@protocol(HeroDelegate)]) {
        [self.delegate heroReturn:self toTheBoat:boat];
    }
}

- (void)moveToTheBoat:(Boat *)boat from:(UIView *)view {
    if ([self.delegate conformsToProtocol:@protocol(HeroDelegate)]) {
        [self.delegate heroMove:self toTheBoat:boat from:view];
    }
}

- (void)moveToTheLeftShore:(UIView *)view fromBoat:(Boat *)boat {
    if ([self.delegate conformsToProtocol:@protocol(HeroDelegate)]) {
        [self.delegate heroMove:self toTheLeftHore:view fromBoat:boat];
    }
}

- (void)moveToTheRightShore:(UIView *)view fromBoat:(Boat *)boat {
    if ([self.delegate conformsToProtocol:@protocol(HeroDelegate)]) {
        [self.delegate heroMove:self toTheRightHore:view fromBoat:boat];
    }
}

- (void)setLeftShorePosition:(CGPoint)leftShorePosition {
    CGRect frame = self.frame;
    frame.origin = leftShorePosition;
    _leftShorePosition = leftShorePosition;
    self.frame = frame;
}

- (NSMutableArray *)getDefaultConstraintsFromView:(UIView *)view {
    NSMutableArray *constraints = [NSMutableArray new];
    for (NSLayoutConstraint *con in view.constraints) {
        if ([con.firstItem isEqual:self] || [con.secondItem isEqual:self]) {
            [constraints addObject:con];
        }
    }
    
    return constraints;
}

- (NSMutableArray *)setConstraintsForRightShore:(UIView *)view {
    
    NSMutableArray *constraints = [NSMutableArray array];
    
    for (NSLayoutConstraint *constraint in self.leftShoreConstraints) {
        if ([constraint.firstItem isEqual:self] || [constraint.secondItem isEqual:self]) {
            
            if (constraint.firstAttribute == NSLayoutAttributeLeading || constraint.secondAttribute == NSLayoutAttributeLeading) {
                [constraints addObject:[NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeTrailing multiplier:1.f constant:0 - constraint.constant]];
            }
            
            if (constraint.firstAttribute == NSLayoutAttributeBottom || constraint.secondAttribute == NSLayoutAttributeBottom) {
                [constraints addObject:constraint];
            }
        }
        
    }
    
    return constraints;
}

- (void)setLeadingConstraint:(CGFloat)constant toBoat:(Boat *)boat {
    NSLayoutConstraint *leading = [NSLayoutConstraint
                                   constraintWithItem:self
                                   attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:boat
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1.0
                                   constant:constant];
    
    [self.boatConstraints addObject:leading];
}

- (void)setTopConstraint:(CGFloat)constant toBoat:(Boat *)boat {
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:boat attribute:NSLayoutAttributeTop multiplier:1 constant:constant];
    
    [self.boatConstraints addObject:top];
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
