//
//  ViewController.h
//  Fairytale
//
//  Created by Zhenya Kaidashov on 12/11/17.
//  Copyright © 2017 Zhenya Kaidashov. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    LeftBoom,
    RightBoom,
} BoomType;

@class Boat;
@class Goat;
@class Wolf;
@class Cabbage;

@interface ViewController : UIViewController <UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *leftShoreView;
@property (weak, nonatomic) IBOutlet UIImageView *rightShoreView;
@property (weak, nonatomic) IBOutlet UIImageView *riverView;

@property (weak, nonatomic) IBOutlet Boat *boatView;
@property (weak, nonatomic) IBOutlet Goat *goatView;
@property (weak, nonatomic) IBOutlet Wolf *wolfView;
@property (weak, nonatomic) IBOutlet Cabbage *cabbageView;

@property (weak, nonatomic) IBOutlet UIView *startGamePopupCloudView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *startGamePopupCloudViewVerticalConstraint;

@property (weak, nonatomic) IBOutlet UIView *finishedPopupCloudView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *finishedPopupCloudViewVerticalConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *finishedPopupCloudViewImage;
@property (weak, nonatomic) IBOutlet UILabel *finishedPopupCloudViewLabel;

@property (weak, nonatomic) IBOutlet UIImageView *leftBoomView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftBoomViewBottomConstraint;

@property (weak, nonatomic) IBOutlet UIImageView *rightBoomView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightBoomViewBottomConstraint;

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *skyCloudsViews;

- (IBAction)actionGo:(UIButton *)sender;
- (IBAction)actionRestart:(UIButton *)sender;


@end

