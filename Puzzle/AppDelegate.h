//
//  AppDelegate.h
//  Fairytale
//
//  Created by Zhenya Kaidashov on 12/11/17.
//  Copyright © 2017 Zhenya Kaidashov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

