//
//  ViewController.m
//  Fairytale
//
//  Created by Zhenya Kaidashov on 12/11/17.
//  Copyright © 2017 Zhenya Kaidashov. All rights reserved.
//

#import "ViewController.h"
#import "Boat.h"
#import "Goat.h"
#import "Wolf.h"
#import "Cabbage.h"
#import "HeroHandler.h"

@interface ViewController ()

@property (assign, nonatomic) CGFloat leftBorder;
@property (assign, nonatomic) CGFloat rightBorder;

@property (assign, nonatomic) HeroType leftShoreHeros;
@property (assign, nonatomic) HeroType rightShoreHeros;

@property (weak, nonatomic) Hero *draggingHero;
@property (assign, nonatomic) CGPoint draggingHeroCurrentPosition;

@property (strong, nonatomic) HeroHandler *handler;

@property (assign, nonatomic) CGFloat defaultBoatLeading;
@property (assign, nonatomic) CGFloat endBoatLeading;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Init hero action handler
    self.handler = [[HeroHandler alloc] init];
    
    // Set delegate for heroes
    self.wolfView.delegate = self.handler;
    self.goatView.delegate = self.handler;
    self.cabbageView.delegate = self.handler;
    
    // Set all heroes on the left shore
    self.leftShoreHeros = WolfHero | GoatHero | CabbageHero;
    
    [self hideHeroesAndBoat];
    
    [self showCloudPopup:self.startGamePopupCloudView withChangeVerticalConstraint:self.startGamePopupCloudViewVerticalConstraint andAnimated:NO];
    
    self.leftBoomView.alpha = 0;
    self.rightBoomView.alpha = 0;
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    panGesture.delegate = self;
    panGesture.maximumNumberOfTouches = 1;
    [self.view addGestureRecognizer:panGesture];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(boatGoToTheLeftShore:) name:BoatGoToTheLeftShoreNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(boatGoToTheRightShore:) name:BoatGoToTheRightShoreNotification object:nil];
    
    [self skyCloudsAnimation];
}

#pragma mark - Animations
- (void)skyCloudsAnimation {
    
    void(^__block down)(UIImageView *, CGFloat);
    void(^__block up)(UIImageView *, CGFloat);
    
    down = ^(UIImageView *cloudView, CGFloat duration) {
        __block CGRect frame = cloudView.frame;
        frame.origin.y = frame.origin.y - 5;
        
        [UIView animateWithDuration:duration animations:^{
            cloudView.frame = frame;
        } completion:^(BOOL finished) {
            up(cloudView, duration);
        }];
    };
    
    
    
    up = ^(UIImageView *cloudView, CGFloat duration) {
        __block CGRect frame = cloudView.frame;
        frame.origin.y = frame.origin.y + 5;
        
        [UIView animateWithDuration:duration animations:^{
            cloudView.frame = frame;
        } completion:^(BOOL finished) {
            down(cloudView, duration);
        }];
    };
    
    for (UIImageView *cloudView in self.skyCloudsViews) {
        CGFloat duration = ((arc4random() % 11) / 10.f ) + 1.0;
        up(cloudView, duration);
    }
}

- (void)scaleSelectedHero:(UIView *)hero byValue:(CGFloat)scaleValue {
    [UIView animateWithDuration:0.3 animations:^{
        hero.transform = CGAffineTransformMakeScale(scaleValue, scaleValue);
    }];
}

#pragma mark - Boat methods
- (void)boatGoToTheLeftShore:(NSNotification *)notification {
    [self changeBoatLeading:self.defaultBoatLeading];
    
    if ((self.rightShoreHeros & GoatHero) && (self.rightShoreHeros & CabbageHero)) {
        [self showBoom:self.rightBoomView withType:RightBoom];
        [self goatAteCabbage];
    } else if ((self.rightShoreHeros & GoatHero) && (self.rightShoreHeros & WolfHero)) {
        [self showBoom:self.rightBoomView withType:RightBoom];
        [self wolfAteGoat];
    }
}

- (void)boatGoToTheRightShore:(NSNotification *)notification {
    [self changeBoatLeading:self.endBoatLeading];
    
    if ((self.leftShoreHeros & GoatHero) && (self.leftShoreHeros & CabbageHero)) {
        [self showBoom:self.leftBoomView withType:LeftBoom];
        [self goatAteCabbage];
    } else if ((self.leftShoreHeros & GoatHero) && (self.leftShoreHeros & WolfHero)) {
        [self showBoom:self.leftBoomView withType:LeftBoom];
        [self wolfAteGoat];
    }
}

- (void)changeBoatLeading:(CGFloat)constant {
    for (NSLayoutConstraint *constraint in self.view.constraints) {
        if ([constraint.firstItem isEqual:self.boatView] || [constraint.secondItem isEqual:self.boatView]) {
            if (constraint.firstAttribute == NSLayoutAttributeLeading || constraint.secondAttribute == NSLayoutAttributeLeading) {
                constraint.constant = constant;
            }
        }
    }
}

#pragma mark - Methods of result
- (void)goatAteCabbage {
    self.finishedPopupCloudViewImage.image = [UIImage imageNamed:@"goat-ate-cabbage.png"];
    self.finishedPopupCloudViewLabel.text = @"Кози — безневинні няшки лише на перший погляд. А насправді вони тільки й чекають, поки ти врятуєш їх від Вовка – щоб потім, поки ніхто не бачить, тихенько схрумати твою капусту.";
    
    [self hideHeroesAndBoat];
    [self showCloudPopup:self.finishedPopupCloudView withChangeVerticalConstraint:self.finishedPopupCloudViewVerticalConstraint andAnimated:YES];
}

- (void)wolfAteGoat {
    self.finishedPopupCloudViewImage.image = [UIImage imageNamed:@"wolf-ate-goat.png"];
    self.finishedPopupCloudViewLabel.text = @"Упс! Здається, щойно Вовк безцеремонно зжер козу. Мирно пливти на крижині, споглядаючи зимові краєвиди — це звісно, чудово, але ж і про харчовий ланцюг забувати не слід!";
    
    [self hideHeroesAndBoat];
    [self showCloudPopup:self.finishedPopupCloudView withChangeVerticalConstraint:self.finishedPopupCloudViewVerticalConstraint andAnimated:YES];
}

- (void)win {
    self.finishedPopupCloudViewImage.image = [UIImage imageNamed:@"win.png"];
    self.finishedPopupCloudViewLabel.text = @"Геніально! Респект за бездоганне вирішення складної задачі!";
    
    [self hideHeroesAndBoat];
    [self showCloudPopup:self.finishedPopupCloudView withChangeVerticalConstraint:self.finishedPopupCloudViewVerticalConstraint andAnimated:YES];
}

- (void)showBoom:(UIImageView *)boomView withType:(BoomType)type {
    NSUInteger n = 8;
    boomView.alpha = 1;
    
    NSLayoutConstraint *constraint;
    
    if (type == LeftBoom) {
        constraint =  [self valueForKey:@"leftBoomViewBottomConstraint"];
    } else {
        constraint =  [self valueForKey:@"rightBoomViewBottomConstraint"];
    }
    
    CGFloat defaulValue = constraint.constant;
    while (n != 0) {
        
        if (n == 8) {
            constraint.constant = defaulValue + n;
        } else {
            constraint.constant = constraint.constant + n * 2;
        }
        
        
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            constraint.constant = constraint.constant - n * 2;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            }];
        }];
        
        n--;
    }
}

- (void)hideBoom:(UIImageView *)boomView {
    boomView.alpha = 0;
}

#pragma mark - Hide and show heroes and boat methods
- (void)showHeroesAndBoat {
    self.wolfView.hidden = NO;
    self.goatView.hidden = NO;
    self.cabbageView.hidden = NO;
    self.boatView.hidden = NO;
}

- (void)hideHeroesAndBoat {
    self.wolfView.hidden = YES;
    self.goatView.hidden = YES;
    self.cabbageView.hidden = YES;
    self.boatView.hidden = YES;
}

#pragma mark - Cloud popup methods
- (void)showCloudPopup:(UIView *)popup withChangeVerticalConstraint:(NSLayoutConstraint *)constraint andAnimated:(BOOL)animated {
    popup.hidden = NO;
    constraint.constant = 0;
    
    if (animated) {
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

- (void)hideCloudPopup:(UIView *)popup withChangeVerticalConstraint:(NSLayoutConstraint *)constraint andAnimated:(BOOL)animated {
    constraint.constant = -1 * CGRectGetMaxY(popup.frame);
    
    if (animated) {
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            popup.hidden = YES;
        }];
    } else {
        [self.view layoutIfNeeded];
    }
}

- (IBAction)actionGo:(UIButton *)sender {
    [self hideCloudPopup:self.startGamePopupCloudView withChangeVerticalConstraint:self.startGamePopupCloudViewVerticalConstraint andAnimated:YES];
    [self showHeroesAndBoat];
}

- (IBAction)actionRestart:(UIButton *)sender {
    [self.wolfView removeFromSuperview];
    [self.cabbageView removeFromSuperview];
    [self.goatView removeFromSuperview];
    
    self.wolfView.location = self.cabbageView.location = self.goatView.location = OnTheLeftShore;
    
    self.leftShoreHeros = self.leftShoreHeros = WolfHero | GoatHero | CabbageHero;
    self.rightShoreHeros = 0;
    
    self.boatView.location = AtTheLeftShore;
    [self changeBoatLeading:self.defaultBoatLeading];
    
    [self.view addSubview:self.wolfView];
    [self.view addConstraints:self.wolfView.leftShoreConstraints];
    [self.view addSubview:self.cabbageView];
    [self.view addConstraints:self.cabbageView.leftShoreConstraints];
    [self.view addSubview:self.goatView];
    [self.view addConstraints:self.goatView.leftShoreConstraints];
    [self.view layoutIfNeeded];
    
    [self hideBoom:self.leftBoomView];
    [self hideBoom:self.rightBoomView];
    [self hideCloudPopup:self.finishedPopupCloudView withChangeVerticalConstraint:self.finishedPopupCloudViewVerticalConstraint andAnimated:YES];
    
    [self showHeroesAndBoat];
}

#pragma mark - Touches methods
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self.view];
    UIView *view = [self.view hitTest:point withEvent:event];
    
    if ([view isKindOfClass:[Hero class]]) {
        [self scaleSelectedHero:view byValue:1.1f];
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self.view];
    UIView *view = [self.view hitTest:point withEvent:event];
    
    if ([view isKindOfClass:[Hero class]]) {
        [self scaleSelectedHero:view byValue:1.f];
    }
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self.view];
    UIView *view = [self.view hitTest:point withEvent:event];
    
    if ([view isKindOfClass:[Hero class]]) {
        [self scaleSelectedHero:view byValue:1.f];
    }
}

#pragma mark - Handle pan gesture
- (void)handlePan:(UIPanGestureRecognizer *)panGesture {
    
    CGPoint pointOnBasicView = [panGesture locationInView:self.view];
    UIGestureRecognizerState gestureState = panGesture.state;
    
    if (gestureState == UIGestureRecognizerStateBegan) {
        UIView *view = [self.view hitTest:pointOnBasicView withEvent:nil];
        
        if ([view isKindOfClass:[Hero class]]) {
            self.draggingHero = (Hero *)view;
        }
        
    }
    
    if (gestureState == UIGestureRecognizerStateEnded || gestureState == UIGestureRecognizerStateCancelled || gestureState == UIGestureRecognizerStateFailed) {
        
        if (self.draggingHero) {
            [self scaleSelectedHero:self.draggingHero byValue:1.f];
            [self handleTheEndOfTheDraggingHero];
            self.draggingHero = nil;
            self.draggingHeroCurrentPosition = CGPointZero;
        }
        
    }
    
    if (self.draggingHero && [[self.draggingHero superview] isEqual:self.boatView]) {
        pointOnBasicView = [self.view convertPoint:pointOnBasicView toView:self.boatView];
    }
    
    if (self.draggingHero) {
        self.draggingHero.center = pointOnBasicView;
    }
    
    if ([[self.draggingHero superview] isEqual:self.boatView]) {
        self.draggingHeroCurrentPosition = [self.boatView convertPoint:pointOnBasicView toView:self.view];
    } else {
        self.draggingHeroCurrentPosition = pointOnBasicView;
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRequireFailureOfGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return [gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UISwipeGestureRecognizer class]];
}

#pragma mark - Handle the end of dragging hero
- (void)handleTheEndOfTheDraggingHero {
    
    if (self.boatView.location == AtTheLeftShore && self.draggingHero.location == OnTheRightShore) {
        [self.draggingHero returnToTheRightShore:self.view];
        return;
    } else if (self.boatView.location == AtTheRightShore && self.draggingHero.location == OnTheLeftShore) {
        [self.draggingHero returnToTheLeftShore:self.view];
        return;
    }
    
    if (self.boatView.location == AtTheLeftShore) {
        
        if (self.draggingHeroCurrentPosition.x < self.leftBorder) {
            
            if ([[self.draggingHero superview] isEqual:self.boatView]) {
                [self.draggingHero moveToTheLeftShore:self.view fromBoat:self.boatView];
            } else {
                [self.draggingHero returnToTheLeftShore:self.view];
            }
            
        } else {
            
            if ([[self.draggingHero superview] isEqual:self.boatView]) {
                [self.draggingHero returnToTheBoat:self.boatView];
            } else if ([self.boatView.subviews count] > 0) {
                Hero *view = [self.boatView subviews][0];
                [view moveToTheLeftShore:self.view fromBoat:self.boatView];
                
                if (!(self.leftShoreHeros & view.type)) {
                    self.leftShoreHeros = self.leftShoreHeros | view.type;
                }
                
                [self.draggingHero moveToTheBoat:self.boatView from:self.view];
            } else {
                [self.draggingHero moveToTheBoat:self.boatView from:self.view];
            }
        }
        
    } else if (self.boatView.location == AtTheRightShore) {
        if (self.draggingHeroCurrentPosition.x > self.rightBorder) {
            
            if ([[self.draggingHero superview] isEqual:self.boatView]) {
                [self.draggingHero moveToTheRightShore:self.view fromBoat:self.boatView];
            } else {
                [self.draggingHero returnToTheRightShore:self.view];
            }
            
        } else {
            
            if ([[self.draggingHero superview] isEqual:self.boatView]) {
                [self.draggingHero returnToTheBoat:self.boatView];
            } else if ([self.boatView.subviews count] > 0) {
                Hero *view = [self.boatView subviews][0];
                [view moveToTheRightShore:self.view fromBoat:self.boatView];
                
                if (!(self.rightShoreHeros & view.type)) {
                    self.rightShoreHeros = self.rightShoreHeros | view.type;
                }
                
                [self.draggingHero moveToTheBoat:self.boatView from:self.view];
            } else {
                [self.draggingHero moveToTheBoat:self.boatView from:self.view];
            }
        }
    }
    
    switch (self.draggingHero.location) {
        case OnTheLeftShore:
            if (!(self.leftShoreHeros & self.draggingHero.type)) {
                self.leftShoreHeros = self.leftShoreHeros | self.draggingHero.type;
            }
            break;
        case InTheBoat:
            if (self.leftShoreHeros & self.draggingHero.type) {
                self.leftShoreHeros = self.leftShoreHeros ^ self.draggingHero.type;
            } else if (self.rightShoreHeros & self.draggingHero.type) {
                self.rightShoreHeros = self.rightShoreHeros ^ self.draggingHero.type;
            }
            break;
        case OnTheRightShore:
            if (!(self.rightShoreHeros & self.draggingHero.type)) {
                self.rightShoreHeros = self.rightShoreHeros | self.draggingHero.type;
            }
            break;
    }
    
    if ((self.rightShoreHeros & WolfHero) && (self.rightShoreHeros & GoatHero) && (self.rightShoreHeros & CabbageHero)) {
        [self win];
    }
    
}

#pragma mark - View methods
- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    self.wolfView.translatesAutoresizingMaskIntoConstraints = NO;
    self.goatView.translatesAutoresizingMaskIntoConstraints = NO;
    self.cabbageView.translatesAutoresizingMaskIntoConstraints = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // Set left and right border
    self.leftBorder = CGRectGetMaxX(self.leftShoreView.frame);
    self.rightBorder = CGRectGetMinX(self.rightShoreView.frame);
    
    [self.boatView setStartAndFinishMovementPositionBetweenLeft:self.leftShoreView andRightShores:self.rightShoreView];
    
    // Initialize constraints and positions for Wolf
    self.wolfView.leftShorePosition = CGPointMake(CGRectGetMinX(self.wolfView.frame), CGRectGetMinY(self.wolfView.frame));
    self.wolfView.rightShorePosition = CGPointMake(CGRectGetMaxX(self.view.bounds) - CGRectGetMaxX(self.wolfView.frame), CGRectGetMinY(self.wolfView.frame));
    self.wolfView.boatPosition = CGPointMake(10, CGRectGetMaxY(self.boatView.bounds) - (CGRectGetHeight(self.wolfView.frame) + 40));
    self.wolfView.leftShoreConstraints = [self.wolfView getDefaultConstraintsFromView:self.view];
    self.wolfView.rightShoreConstraints = [self.wolfView setConstraintsForRightShore:self.view];
    [self.wolfView setLeadingConstraint:self.wolfView.boatPosition.x toBoat:self.boatView];
    [self.wolfView setTopConstraint:self.wolfView.boatPosition.y toBoat:self.boatView];
    
    // Initialize constraints and positions for Goat
    self.goatView.leftShorePosition = CGPointMake(CGRectGetMinX(self.goatView.frame), CGRectGetMinY(self.goatView.frame));
    self.goatView.rightShorePosition = CGPointMake(CGRectGetMaxX(self.view.bounds) - CGRectGetMaxX(self.goatView.frame), CGRectGetMinY(self.goatView.frame));
    self.goatView.boatPosition = CGPointMake(10, CGRectGetMaxY(self.boatView.bounds) - (CGRectGetHeight(self.goatView.frame) + 40));
    self.goatView.leftShoreConstraints = [self.goatView getDefaultConstraintsFromView:self.view];
    self.goatView.rightShoreConstraints = [self.goatView setConstraintsForRightShore:self.view];
    [self.goatView setLeadingConstraint:self.goatView.boatPosition.x toBoat:self.boatView];
    [self.goatView setTopConstraint:self.goatView.boatPosition.y toBoat:self.boatView];
    
    // Initialize constraints and positions for Cabbage
    self.cabbageView.leftShorePosition = CGPointMake(CGRectGetMinX(self.cabbageView.frame), CGRectGetMinY(self.cabbageView.frame));
    self.cabbageView.rightShorePosition = CGPointMake(CGRectGetMaxX(self.view.bounds) - CGRectGetMaxX(self.cabbageView.frame), CGRectGetMinY(self.cabbageView.frame));
    self.cabbageView.boatPosition = CGPointMake(40, CGRectGetMaxY(self.boatView.bounds) - (CGRectGetHeight(self.cabbageView.frame) + 50));
    self.cabbageView.leftShoreConstraints = [self.cabbageView getDefaultConstraintsFromView:self.view];
    self.cabbageView.rightShoreConstraints = [self.cabbageView setConstraintsForRightShore:self.view];
    [self.cabbageView setLeadingConstraint:self.cabbageView.boatPosition.x toBoat:self.boatView];
    [self.cabbageView setTopConstraint:self.cabbageView.boatPosition.y toBoat:self.boatView];
    
    for (NSLayoutConstraint *constraint in self.view.constraints) {
        
        if ([constraint.firstItem isEqual:self.boatView] || [constraint.secondItem isEqual:self.boatView]) {
            if (constraint.firstAttribute == NSLayoutAttributeLeading || constraint.secondAttribute == NSLayoutAttributeLeading) {
                self.defaultBoatLeading = constraint.constant;
                self.endBoatLeading = (CGRectGetMinX(self.rightShoreView.frame) - CGRectGetMaxX(self.leftShoreView.frame) - CGRectGetWidth(self.boatView.frame)) + 10;
            }
        }
        
    }
    
    [self hideCloudPopup:self.finishedPopupCloudView withChangeVerticalConstraint:self.finishedPopupCloudViewVerticalConstraint andAnimated:NO];
}

#pragma mark - Dealloc method

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Other methods

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}
@end
