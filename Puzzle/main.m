//
//  main.m
//  Fairytale
//
//  Created by Zhenya Kaidashov on 12/11/17.
//  Copyright © 2017 Zhenya Kaidashov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
