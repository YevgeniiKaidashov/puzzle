//
//  HeroHandler.h
//  Fairytale
//
//  Created by Zhenya Kaidashov on 12/12/17.
//  Copyright © 2017 Zhenya Kaidashov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Hero.h"

@class Boat;

@interface HeroHandler : NSObject <HeroDelegate>

- (void)heroReturn:(Hero *)hero toTheLeftShore:(UIView *)view;
- (void)heroReturn:(Hero *)hero toTheRightShore:(UIView *)view;

- (void)heroMove:(Hero *)hero toTheLeftHore:(UIView *)view fromBoat:(Boat *)boat;
- (void)heroMove:(Hero *)hero toTheRightHore:(UIView *)view fromBoat:(Boat *)boat;

- (void)heroReturn:(Hero *)hero toTheBoat:(Boat *)boat;
- (void)heroMove:(Hero *)hero toTheBoat:(Boat *)boat from:(UIView *)view;

@end
