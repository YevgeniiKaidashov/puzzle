//
//  Boat.h
//  Fairytale
//
//  Created by Zhenya Kaidashov on 12/12/17.
//  Copyright © 2017 Zhenya Kaidashov. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    BoatDirectionLeft,
    BoatDirectionRight,
} BoatDirection;

typedef enum {
    AtTheLeftShore,
    AtTheRightShore,
} BoatLocation;

extern NSString *const BoatGoToTheLeftShoreNotification;
extern NSString *const BoatGoToTheRightShoreNotification;

@interface Boat : UIImageView <UIGestureRecognizerDelegate>

@property (assign, nonatomic) CGFloat startBoatXPosition;
@property (assign, nonatomic) CGFloat endBoatXPosition;
@property (assign, nonatomic) BoatLocation location;
@property (weak, nonatomic) UIPanGestureRecognizer *swipeGesture;
@property (assign, nonatomic, getter=isEmpty) BOOL empty;

- (void)setStartAndFinishMovementPositionBetweenLeft:(UIView *)leftShore andRightShores:(UIView *)rightShore;

@end
